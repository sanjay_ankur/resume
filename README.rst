Ankur Sinha's resume
====================

This is the LaTeX source code for my resume. Feel free to use the template - it's available here: http://www.tedpavlic.com/post_resume_cv_latex_example.php

The actual resume document is under the `Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License (CC by-nc-nd 4.0)`_ 

.. _Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License (CC by-nc-nd 4.0): http://creativecommons.org/licenses/by-nc-nd/4.0/
